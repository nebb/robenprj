<?php 
require'../includes/conn.php';
if (session_status() == PHP_SESSION_NONE) {
  session_start();
}
if(!isset($_SESSION['user'])){
    die();
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Admin</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <?php include('nav.php');?>
<?php 

$st = "SELECT * FROM `tbl_user` WHERE `user`=:u";
$cm = $conn->prepare($st);
$cm->bindParam(':u', $_SESSION['user']);
$cm->execute();
while($row = $cm->fetch(PDO::FETCH_ASSOC)){
$access=$row['access'];
}

?>

<style>
  .breadcrumb{display:none !important;}
</style>
  