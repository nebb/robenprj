

<?php
require'../conn.php';
if ($_SERVER['REQUEST_METHOD']=="POST"){

	$u = strip_tags($_POST['user']);
	$p = strip_tags($_POST['pass']);

	if (isset($u,$p)){

	try {
		$str= "select * from tbl_user where username=:u and password=:p";
		$cm=$conn->prepare($str);
		$cm->bindParam(':u', $u);
		$cm->bindParam(':p', $p);
		$cm->execute();
		$user = $cm->rowcount();
		
		if ($user == 0) {
            ?>
            <script>alert('login failed');</script>
            <?php
			
			
		}else{
            ?>
            <script>alert('success');</script>
            <?php
            $_SESSION['user']= $u;
			header("refresh:1;url=./index.php");
		}


	} catch (Exception $e) {
		echo 'error  '.$e ->getmessage();
	}
	
	}

}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Login</div>
      <div class="card-body">
        <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input class="form-control" id="exampleInputEmail1" type="text" aria-describedby="emailHelp" placeholder="Enter username" name="user">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input name ="pass" class="form-control" id="exampleInputPassword1" type="password" placeholder="Password">
          </div>
          <!-- <div class="form-group">
            <div class="form-check">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox"> Remember Password</label>
            </div>
          </div> -->
          <button class="btn btn-primary btn-block" type="submit">login</button>
        </form>
        <div class="text-center">
          <!-- <a class="d-block small mt-3" href="register.html">Register an Account</a> -->
          <!-- <a class="d-block small" href="forgot-password.html">Forgot Password?</a> -->
        </div>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
