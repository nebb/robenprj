-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2020 at 03:33 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sbdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_emp`
--

CREATE TABLE `tbl_emp` (
  `ID` int(11) NOT NULL,
  `name` text NOT NULL,
  `position` text NOT NULL,
  `content` text NOT NULL,
  `address` text NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_emp`
--

INSERT INTO `tbl_emp` (`ID`, `name`, `position`, `content`, `address`, `image`) VALUES
(1, 'Jennalyn P. Carag', 'Mayor', '<p>Mayor 2016-2019, 2019-2022</p>', '<p>Solana, Cagayan</p>', './img/M.jpg'),
(2, 'Meynard Z.  Carag', 'Vice Mayor', '<p>Vice-Mayor 2016-2022</p>', '<p>Vice-Mayor 2016-2022</p>', './img/VM.jpg'),
(3, 'Francesca F. De Asis', 'Sangunian Bayan', '<p>Sangguniang Bayan Member 2016-2022</p>', '<p>Centro NorthEast, Solana,Cagayan</p>', './img/1.jpg'),
(4, 'Carlo Paolo G. Lasam', 'Sangunian Bayan', '<p>Sangguniang Bayan Member 2019-2022</p>', '<p>Nangalisan, Solana, Cagayan</p>', './img/2.jpg'),
(5, 'Amelia P. Carag', 'Sangunian Bayan', '<p>Sangguniang Bayan Member 2013-2022</p>', '<p>Bauan East, Solana, Cagayan</p>', './img/3.jpg'),
(6, 'James C. Mallillin', 'Sangunian Bayan', '<p>Sanggunaing Bayan Member 2016-2022</p>', '<p>Centro Northwest, Solana, Cagayan</p>', './img/4.jpg'),
(7, 'Ronald C. Cristobal', 'Sangunian Bayan', '<p>Sangguniang Bayan Member 2019-2022</p>', '<p>Parog-Parog, Solana, Cagayan</p>', './img/5.jpg'),
(8, 'Edilberto A. Edillo', 'Sangunian Bayan', '<p>Sangguniang Bayan Member 2019-2022</p>', '<p>Iraga, Solana, Cagayan</p>', './img/6.jpg'),
(9, 'Angelina L. Fernandez', 'Sangunian Bayan', '<p>Sanggunian Bayan Member 2019-2022</p>', '<p>Carilucud, Solana, Cagayan</p>', './img/7.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_franchisee`
--

CREATE TABLE `tbl_franchisee` (
  `FID` int(11) NOT NULL,
  `name` text NOT NULL,
  `address` text NOT NULL,
  `birthday` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_logs`
--

CREATE TABLE `tbl_logs` (
  `ID` int(11) NOT NULL,
  `RID` int(11) NOT NULL,
  `TRANSACTION` text NOT NULL,
  `STATUS` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post`
--

CREATE TABLE `tbl_post` (
  `ID` int(11) NOT NULL,
  `title` text NOT NULL,
  `date` text NOT NULL,
  `content` text NOT NULL,
  `author` text NOT NULL,
  `image` text NOT NULL,
  `type` text NOT NULL,
  `icon` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_post`
--

INSERT INTO `tbl_post` (`ID`, `title`, `date`, `content`, `author`, `image`, `type`, `icon`) VALUES
(24, 'Ordinance', '0909-2020-19191919', '<p>Municipal Ordinance No. 81</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', 'admin', './img/logo.png', 'aboutpage', 'fa-file'),
(25, 'Cizitens Charter', '0909-2020-19191919', '<p>Municipal Budget Office</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', 'admin', './img/logo.png', 'aboutpage', 'fa-users'),
(27, 'Demographic', '0909-1717-19191919', '<p>Population</p>\r\n<p>Barangays</p>\r\n<p>Climates</p>', 'admin', './img/logo.png', 'aboutpage', 'fa-globe'),
(35, 'History', '0909-2020-19191919', '<p>Solana Hymn</p>\r\n<p>History</p>', 'admin', './img/logo.png', 'aboutpage', 'fa-history'),
(36, 'Scholarship', '2019-02-09', '<p>TO ALL SOLANIAN COLLEGE STUDENTS, APPLICATION FOR LGU SCHOLARS IS NOW OPEN</p>\r\n<p>REQUIREMENTS:<br />1. BARANGAY CLEARANCE<br />2. CERTIFICATION OF GRADE (average must be 83% and above)<span class=\"text_exposed_show\"><br />3. ASSESSMENT<br />4. RECEIPT UPON ENROLLMENT</span></p>', 'admin', './img/school-books-500x500.jpg', 'announcement', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_requirements`
--

CREATE TABLE `tbl_requirements` (
  `ID` int(11) NOT NULL,
  `FID` int(11) NOT NULL,
  `BC` text NOT NULL,
  `PC` text NOT NULL,
  `CEDULA` text NOT NULL,
  `LTO` text NOT NULL,
  `UNIT` text NOT NULL,
  `HTLSL` text NOT NULL,
  `INSURANCE` text NOT NULL,
  `PIC` text NOT NULL,
  `PIC2` text NOT NULL,
  `TRASH` text NOT NULL,
  `TIN` text NOT NULL,
  `DL` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `ID` int(11) NOT NULL,
  `user` text NOT NULL,
  `pass` text NOT NULL,
  `access` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`ID`, `user`, `pass`, `access`) VALUES
(1, 'admin', 'admin', 'developer'),
(2, 'x', 'x', 'user'),
(3, 'mbo', '1234', 'normal'),
(4, 'abby', 'gail', 'admin'),
(5, 'precious', 'tugaoen', 'normal'),
(6, 'nico', 'nico', 'normal'),
(7, 'precious', 'precious', 'developer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_emp`
--
ALTER TABLE `tbl_emp`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_franchisee`
--
ALTER TABLE `tbl_franchisee`
  ADD PRIMARY KEY (`FID`);

--
-- Indexes for table `tbl_logs`
--
ALTER TABLE `tbl_logs`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_post`
--
ALTER TABLE `tbl_post`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_requirements`
--
ALTER TABLE `tbl_requirements`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_emp`
--
ALTER TABLE `tbl_emp`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_franchisee`
--
ALTER TABLE `tbl_franchisee`
  MODIFY `FID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_logs`
--
ALTER TABLE `tbl_logs`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_post`
--
ALTER TABLE `tbl_post`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `tbl_requirements`
--
ALTER TABLE `tbl_requirements`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
