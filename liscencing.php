<?php include('header.php');?>
<body class="<?php echo basename($_SERVER["SCRIPT_FILENAME"], '.php' );?>">
<?php include('includes/navv.php');?>

<div id="title">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Title</h1>
            </div>
        </div>
    </div>
</div>

<div id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12"><p>
                <span>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Expedita aliquid fuga libero veritatis molestias fugit laboriosam quas. Ea optio dignissimos voluptatibus, repudiandae sunt sed rem ex autem inventore reiciendis quaerat.</span>
                <span>A eligendi aspernatur commodi, dolor quibusdam autem magnam quod tempore, doloremque deleniti minima aliquid voluptatum delectus, atque tempora aut soluta fuga animi quia in illum nisi beatae? Reprehenderit, molestias repellendus!</span>
                <span>Sequi quaerat provident deserunt voluptate tenetur labore nemo sint iusto accusantium velit? Atque doloribus voluptates tempore debitis porro dolores laboriosam delectus minus eos facere obcaecati optio dolor labore, aspernatur dolorem.</span>
                <span>Nesciunt facilis quas quae fugiat nostrum exercitationem error! Dolorem fuga adipisci eaque voluptate quo nesciunt enim. Commodi laboriosam aut et. At doloremque reprehenderit nihil repellat odio fuga et dolor eum.</span>
                <span>Totam accusantium ut veritatis modi, iure, enim placeat corporis labore temporibus incidunt earum ipsam provident fugit commodi eum a! Doloremque consectetur perferendis modi! Labore quaerat delectus mollitia quos recusandae quam.</span>
                <span>Vel nemo suscipit dicta quos doloribus quas totam rerum veritatis? At, ad rem. Quibusdam saepe enim ea praesentium itaque quia atque beatae, dignissimos consequuntur, at earum debitis, aspernatur possimus omnis!</span>
                <span>Facilis non doloribus magni, recusandae laudantium delectus. Maiores, nam excepturi. Aut illo veritatis non ipsum? Officiis praesentium deleniti nobis provident aut nulla odit explicabo illo unde harum. Soluta, reiciendis in!</span>
                <span>Accusantium, repellendus blanditiis! Asperiores esse eum illum porro amet quidem cupiditate? Rem nesciunt odit, sint unde, tempora cumque reprehenderit doloribus obcaecati dolores iure tempore! Quia neque mollitia nulla nemo aut?</span>
                <span>Molestiae aperiam velit dolore laboriosam porro sequi recusandae quos numquam! Quia ea unde itaque fugiat ducimus esse assumenda officia expedita minima harum officiis labore quidem voluptates, nobis repudiandae qui tempore!</span>
                <span>Maiores, corporis! Aspernatur earum iusto voluptate aperiam eaque provident non omnis! Minus aut in provident debitis ex molestias ducimus veritatis, recusandae placeat libero, ad fuga, iusto quo. Nobis, debitis quasi.</span>
                <span>Reprehenderit aliquam voluptatum nam cumque quaerat, rem pariatur. Quidem tempora quas sed tempore itaque fugiat mollitia nisi unde molestias dolore, dolorem illo eius vero a enim reprehenderit suscipit corrupti ab.</span>
                <span>Deserunt quia pariatur animi inventore rerum ipsa blanditiis temporibus expedita amet suscipit voluptates eaque placeat nemo, dolorem ratione et ullam eum sit, sed odio. Explicabo dolore pariatur velit odio consequuntur.</span>
            </p></div>
        </div>
      
        <div class="row" style="margin-bottom:30px;">
        <div class="col-md-6"> <img src="http://placehold.it/500x500" alt="" class="img img-fluid"> </div>
        <div class="col-md-6"><h2>Title</h2><p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Illo et esse eum eligendi maxime minus, excepturi aliquid quod unde? Porro accusantium atque reiciendis repellat eaque animi a quod earum hic!</p></div>
        </div>


        <div class="row" style="margin-bottom:30px;">
        <div class="col-md-6"> <img src="http://placehold.it/500x500" alt="" class="img img-fluid"> </div>
        <div class="col-md-6"><h2>Title</h2><p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Illo et esse eum eligendi maxime minus, excepturi aliquid quod unde? Porro accusantium atque reiciendis repellat eaque animi a quod earum hic!</p></div>
        </div>


    </div>
    
</div>


 <?php include('footer.php');?>