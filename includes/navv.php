<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
  <a class="navbar-brand" href="./">
  <img src="./img/logo.png" width="90px" height="90px"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="./">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#about">About</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#Announcement">Announcement</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#Resources">Resources</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#Officials">Officials</a>
      </li>


      <?php 
        if(isset($_SESSION['user'])){}else{
          ?>
          <li class="nav-item">
          <a class="nav-link" href="login.php">Login</a>
          </li>
          <?php
        }
      ?>
      
      <!-- <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown link
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#">Pogi</a>
          <a class="dropdown-item" href="#">Mas Pogi</a>
          <a class="dropdown-item" href="#">Ako sayo</a>
        </div>
      </li> -->
    </ul>
  </div>
  </div>
</nav>