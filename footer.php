

<footer>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p> Copyright &copy; <?php echo date('Y');?> | SB Project | Thesis</p>
            </div>
            <div class="col-md-6">
                <!-- <nav class="nav">
                <a class="nav-link active" href="#">Active</a>
                <a class="nav-link" href="#">Link</a>
                <a class="nav-link" href="#">Link</a>
                <a class="nav-link disabled" href="#">Disabled</a> -->
                </nav>
            </div>
        </div>
    </div>

</footer>



<!-- scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="assets/js/app.js"></script>
</body>
</html>