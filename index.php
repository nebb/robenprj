<?php include('header.php');?>
<body class="<?php echo basename($_SERVER["SCRIPT_FILENAME"], '.php' );?>">
<?php include('includes/navv.php');?>

<div class="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>Municipality of Solana</h1>
                <p>Solana, the land beneath the verdant hills</p>
            </div>
        </div>
    </div>
</div>

<div class="section1 spacing" id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="dividerx">
                    <h2>About</h2>
                </div>
            </div>
        </div>

		<div class="row m-top-20">
		<?php 
 $st = "SELECT * FROM `tbl_post` WHERE `type`='aboutpage' limit 4 ";
 $cm = $conn->prepare($st);
 $cm->execute();
 while($row = $cm->fetch(PDO::FETCH_ASSOC)){
?>

			<div class="col-sm-6 col-md-3">
                    <div class="col-md-12 feature-box">
                    <span class="fa <?php echo $row['icon'];?> icon"></span>
                    <h4><?php echo $row['title']?></h4>
                    <p><?php echo limit_string($row['content'],100);?></p>
                    <a href="viewmore.php?id=<?php echo $row['ID'];?>">view more</a>
                </div>
            </div>


 <?php } ?>
		</div>
</div>
        
    </div>
</div>



<div class="section2" >
    <div class="container">
        <div class="row">
            <div class="col-md-6">
				<h2>Vision</h2>
				<p>Solana as the rice bowl and heart of economic activities in southern cagayan empowered with healthy and God-living citizenry, living in an environment - friendly, progressive, safe and peaceful community inspired by dynamic and responsible publoc trustees.</p>
			</div>
			<div class="col-md-6">
				<h2>Mission</h2>
				<p>The Municipality shall provide its constituents better infrastructure and facilities for the development of the agricultural sector to provide more employment opportunities that shall alleviate their living condition to implement more programs for the protection and promotion of an ecologically - balanced and viable environment..</p>
			</div>
        </div>
    </div>
</div>

<!-- announcements -->
<div class="section5 spacing" id="Announcement">

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Announcements</h2>
			</div>
		</div>


<div class="row">
<?php 
 $st = "SELECT * FROM `tbl_post` WHERE `type`='announcement' limit 5 ";
 $cm = $conn->prepare($st);
 $cm->execute();
 while($row = $cm->fetch(PDO::FETCH_ASSOC)){
?>

			<div class="col-md-6">
				<div class="row">
						<div class="col-md-6"><img src="<?php echo $row['image'];?>" alt="" class="img-fluid"></div>
						<div class="col-md-6">
						<h2><?php echo $row['title'];?></h2>
						<hr>
						<?php echo $row['content'];?>
						<a href="viewmore.php?id=<?php echo $row['ID'];?>">view more</a>
						</div>
				</div>
			</div>


 <?php } ?>
 </div>


		

		

	</div>

</div>

<!-- end announcements -->

<div class="section4" id="Resources">

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 x1">
			<h1>Gallery</h1>
			<p>an intelligent photo gallery that learns what is most important to you, organizes your photos & videos into moments.hkjeijbdgeiugdehdiepjdjjkhekkbhgfuker hebg </p>
			<a href="#" class="btn btn-lg btn-warning">CLICK</a>		
			</div>

			<div class="col-md-3 x2">
			<h1>Liscencing</h1>
			<p> A licensing agreement is a written contract between two parties, in which a property owner permits another party to use that property under a specific set of parameters.</p>
			<a href="liscencing.php" class="btn btn-lg btn-warning">CLICK</a>
			</div>

			<div class="col-md-3 x3">
			<h1>Franchising</h1>
			<p>form of business by which the owner of a product, service or method obtains distribution through affiliated dealers kjljsdlihgflsdncjsiohgldskn,cbdksgfkbcbksgkufgkjjhiiug hguyuyjhjfvjhvjh</p>
			<a href="franchising.php" class="btn btn-lg btn-warning">CLICK</a>
			</div>

			<div class="col-md-3 x4">
			<h1>Online Application / Renewal</h1>
			<p>This is were the user apply for renewal and application ..</p>
			<a href="application.php" class="btn btn-lg btn-warning">CLICK</a>
			</div>
		</div>
	</div>

</div>


<div class="section3" id="Officials">

	

<div class="box">
    <div class="container">
	<div class="row">
			<div class="col-md-12 text-center">
				<h2 class="title">Officials</h2>
				<hr>
			</div>
		</div>
     	<div class="row">
		
			

			<?php 
			$st = "SELECT * FROM `tbl_emp`";
			$cm = $conn->prepare($st);
			$cm->execute();
			while($row = $cm->fetch(PDO::FETCH_ASSOC)){


				if($row['position']=="Mayor" or $row['position']=="Vice Mayor" ){
					?>
			<!-- official layout	 -->
			<div class="col-md-6" style="margin-bottom:10px; padding:100px; padding-top:10px;">
			<div class="card" >
			<img class="card-img-top" src="<?php echo $row['image'];?>" alt="Card image" style="width:100%;margin-bottom:0">
			<div class="card-body">
				<h4 class="card-title" style="margin-top:0"><?php echo $row['name'];?></h4>
				<h6><?php echo $row['position'];?></h6>
				<p class="card-text"><?php echo limit_string($row['content'],80);?></p>
				
				<a href="viewofficial.php?id=<?php echo $row['ID'];?>" class="btn btn-primary" style="color:#fff;">See profile</a>
			</div>
			</div>
			</div>
			<!-- end layout -->
					<?php
				}else{
					?>
					<!-- official layout	 -->
					<div class="col-md-3" style="margin-bottom:10px;">
					<div class="card" >
					<img class="card-img-top" src="<?php echo $row['image'];?>" alt="Card image" style="width:100%;margin-bottom:0">
					<div class="card-body">
						<h4 class="card-title" style="margin-top:0"><?php echo $row['name'];?></h4>
						<h6><?php echo $row['position'];?></h6>
						<p class="card-text"><?php echo limit_string($row['content'],200);?></p>
						
						<a href="viewofficial.php?id=<?php echo $row['ID'];?>" class="btn btn-primary" style="color:#fff;">See profile</a>
					</div>
					</div>
					</div>
					<!-- end layout -->
					<?php
				}

			?>

			


			<?php } ?>
		
		</div>		
    </div>
</div>
</div>
 <?php include('footer.php');?>