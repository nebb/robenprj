<?php include('header.php');?>
<?php 
session_start();
if(isset($_SESSION['user'])){
	header("refresh:0;url=admin");
}
?>
<body class="<?php echo basename($_SERVER["SCRIPT_FILENAME"], '.php' );?>">
<?php include('includes/navv.php');?>
<!--Fontawesome CDN-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">


<?php 
if($_SERVER['REQUEST_METHOD']=="POST"):

  $u = strip_tags($_POST['user']);
	$p = strip_tags($_POST['pass']);

	if (isset($u,$p)){

	try {
		$str= "select * from tbl_user where user=:u and pass=:p";
		$cm=$conn->prepare($str);
		$cm->bindParam(':u', $u);
		$cm->bindParam(':p', $p);
		$cm->execute();
		$user = $cm->rowcount();
		
		if ($user == 0) {
			echo "login fail";
			header("refresh:1;url=./");
			
		}else{
      
      $_SESSION['user'] = $u;
      echo $_SESSION['user'] ;

      #die();
      header("refresh:1;url=admin");
    die();
  }


	} catch (Exception $e) {
		echo 'error  '.$e ->getmessage();
	}
	
	}



endif;
?>


<div class="container logincontainer">
	<div class="d-flex justify-content-center h-100">
		<div class="card">
			<div class="card-header">
				<h3>Sign In</h3>
				
			</div>
			<div class="card-body">
				<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
						<input type="text" class="form-control" name="user" placeholder="username">
						
					</div>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input type="password" class="form-control" name="pass" placeholder="password">
					</div>
					<div class="row align-items-center remember">
						<!-- <input type="checkbox">Remember Me -->
					</div>
					<div class="form-group">
						<input type="submit" value="Login" class="btn float-right login_btn">
					</div>
				</form>
			</div>
			<div class="card-footer">
				<!-- <div class="d-flex justify-content-center links">
					Don't have an account?<a href="#">Sign Up</a>
				</div>
				<div class="d-flex justify-content-center">
					<a href="#">Forgot your password?</a>
				</div> -->
			</div>
		</div>
	</div>
</div>



 <?php include('footer.php');?>