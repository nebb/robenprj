$(document).ready(function(){
    //scripts here
    $('a[href^="#"]').on('click', function(event) {

        var target = $( $(this).attr('href') );
    
        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 100);
        }
    
    });


    $('[data-toggle="offcanvas"]').click(function(){
        $("#navigation").toggleClass("hidden-xs");
    });

    //
});