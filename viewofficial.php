<?php 
if(!isset($_GET['id'])){die();}else{


}
?>
<?php include('header.php');?>
<body class="<?php echo basename($_SERVER["SCRIPT_FILENAME"], '.php' );?>">
<?php include('includes/navv.php');?>
<?php 

$st = "select * from tbl_emp where ID=:id";
$cm=$conn->prepare($st);
$cm->bindParam(':id', $_GET['id']);
$cm->execute();
$count = $cm->rowcount();
if($count == 0){
    ?>
        <div class="container m-top-20" style="height:70vh">
            <div class="row" >
                <div class="col-md-12"  style="vertical-align: middle;">
                    <h2>404</h2>
                </div>
            </div>
        </div>
    <?php
}else{
    $st = "SELECT * FROM `tbl_emp` WHERE ID=:id ";
    $cm = $conn->prepare($st);
    $cm->bindParam(':id', $_GET['id']);
    $cm->execute();
    while($row = $cm->fetch(PDO::FETCH_ASSOC)){

        ?>
        <div class="banner" style="background:linear-gradient(rgba(0, 0, 0, 0.7),rgba(0, 0, 0, 0.7));background-size:cover;background-position:center center;">
            <div class="container" style="padding:100px 0;margin:0 auto;">
                
                <div class="row">

                <h1 style="text-align:center;color:#fff;">
                <?php echo $row['name'];?> | <span style="color:#1aa900 !important;"> <?php echo $row['position'];?> </span>
                </h1>
                
                
                </div>
            </div>
        </div>
        <div class="content" style="padding:50px 0;">
        <div class="container">
            <div class="row">
                <div class="col-md-6"><img src="<?php echo $row['image'];?>" alt=""></div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h2>about</h2>
                    <?php echo $row['content'];?>
                </div>
                <div class="col-md-6">
                    <h2>address</h2>
                    <?php echo $row['address'];?>
                </div>
            </div>
        </div>
        </div>
        
        <?php

    //end while
    }
//
}
?>



 <?php include('footer.php');?>